package com.openu.designpattern.adapter;

public class SocketAdapterImpl implements SocketAdapter {
	private Socket sock = new Socket();

	@Override
	public Volt get120Volt() {
		return sock.getVolt();
	}

	@Override
	public Volt get12Volt() {
		Volt v = sock.getVolt();
		return converVolt(v,10);
	}

	@Override
	public Volt get3Volt() {
		Volt v = sock.getVolt();
		return converVolt(v,40);
	}

	private Volt converVolt(Volt v, int i) {
		return new Volt(v.getVolts()/i);
	}
}
