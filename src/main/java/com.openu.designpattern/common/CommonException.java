package com.openu.designpattern.common;

public class CommonException extends Exception {
	private String errorCode;
	
	public CommonException(String message,String errorCode){
		super(message);
		this.errorCode = errorCode;
	}
	public String getErrorCode() {
		return errorCode;

	}
}
