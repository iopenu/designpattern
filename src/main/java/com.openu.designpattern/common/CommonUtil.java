package com.openu.designpattern.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

public class CommonUtil {
	public static String makeUUID(){
		String returnUUID = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
		System.out.println(nowDTFull()+ " : returnUUID = "+returnUUID);
		return returnUUID;
	}
	
	//현재날짜를 구한다.
	public static String nowDATE(){
		String inDate   = new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
		return inDate;
	}
	
	//현재시간을 구한다.
	public static String nowTIME(){
		String inTime   = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		return inTime;
	}
	
	//현재날짜와 시간을 구한다.
	public static String nowDT(){
		Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        String today = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        return today;
	}
	//현재날짜와 시간을 구한다.
	public static String nowDTFull(){
		Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        String today = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date));
        return today;
	}
	//string형태의 숫자를 체크한다.
	public static String checkNumber(String value){
		String returnValue = "0";
		if(StringUtils.isNumeric(value)){
			System.out.println("숫자");
		}else{
			System.out.println("숫자아니네");
		}
		
		return returnValue;
	}

}
