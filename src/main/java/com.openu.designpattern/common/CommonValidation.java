package com.openu.designpattern.common;

public class CommonValidation {
	//여기서 부터 벨리데이션을 체크하는 function
	
	//요청금액 validation errorcode : XXC4
	public static void checkAmount(String amount, String currency) throws CommonException{
		//숫자인지 체크
		if(!NumberUtil.isNumber(amount)){
			throw new CommonException("ERROR - [COMMON_VALIDATE] REQUESTAMOUNT WRONG :"+amount,"XXC4");
		}
		//최소금액 체크
		if(currency.equalsIgnoreCase("JPY") ||currency.equalsIgnoreCase("KRW")){
			//jpy,krw일 경우 소수점 이하 제거 
			amount = NumberUtil.toIntegerNumber(amount);
			
			//JPY일경우 100엔 이상 결제
			if(currency.equalsIgnoreCase("JPY")){
				if(!NumberUtil.compare(amount, "99")){
					throw new CommonException("ERROR - [COMMON_VALIDATE] "+currency+" - REQUEST AMOUNT must be greater than 100 yen. :"+amount,"XXC4");
				}
			}else{
			//KRW일경우 1000원 이상 결제
				if(!NumberUtil.compare(amount, "999")){
					throw new CommonException("ERROR - [COMMON_VALIDATE] "+currency+" - REQUEST AMOUNT must be greater than 1000 won. :"+amount,"XXC4");
				}
			}
		}else{
			//결제금액 1이상만 결제
			if(!NumberUtil.compareDouble(amount, "0.99")){
				throw new CommonException("ERROR - [COMMON_VALIDATE] "+currency+" - REQUEST AMOUNT must be greater than 1 . :"+amount,"XXC4");
			}
		}
	}
	
	//통화 validation errorcode : XXC3
	public static void checkCurrency(String currency) throws CommonException{
		int checkCnt = 0;
		//지원하는 통화
		String currencys[] = { 
				"KRW" ,"USD" ,"JPY" ,"EUR" ,"CNY"
				,"GBP" ,"HKD"
		};
		if(currency.length()!=3){
			throw new CommonException("ERROR - [COMMON_VALIDATE] CURRENCY Illegal Argument :"+currency,"XXC3");
		}
		
		for(String temp:currencys){
			if(temp.equalsIgnoreCase(currency)){
				checkCnt++;
			}
		}
		
		if(checkCnt==0){
			throw new CommonException("ERROR - [COMMON_VALIDATE] CURRENCY Unsupported :"+currency,"XXC3");
		}
		
	}
	
	//requestID validation errorcode : XXC1
	public static void checkRequestId(String requestID) throws CommonException{
		if(requestID.length()!=32){
			throw new CommonException("ERROR - [COMMON_VALIDATE] REQUESTID Illegal Argument :"+requestID,"XXC1");
		}
	}
	
	//통화 userId errorcode : XXC2
	public static void checkUserId(String userID) throws CommonException{
		if(userID.length()<4){
			throw new CommonException("ERROR - [COMMON_VALIDATE] USERID Illegal Argument :"+userID,"XXC2");
		}
	}

}
