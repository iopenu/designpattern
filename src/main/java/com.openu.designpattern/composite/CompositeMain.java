package com.openu.designpattern.composite;

import com.openu.designpattern.composite.baseComponent.Shape;
import com.openu.designpattern.composite.composite.Drawing;
import com.openu.designpattern.composite.leaf.Circle;
import com.openu.designpattern.composite.leaf.Line;
import com.openu.designpattern.composite.leaf.Triangle;

public class CompositeMain {
	public static void main(String[] args) {
		Shape t1 = new Triangle();
		Shape t2 = new Triangle();
		Shape c1 = new Circle();
		Shape l1 = new Line();

		Drawing drawing = new Drawing();
		drawing.add(t1);
		drawing.add(t2);
		drawing.add(c1);
		drawing.add(l1);
		drawing.draw("red");

		drawing.clear();

		drawing.add(t2);
		drawing.add(c1);
		drawing.draw("blue");

	}
}
