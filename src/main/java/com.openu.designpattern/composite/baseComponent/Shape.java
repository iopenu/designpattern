package com.openu.designpattern.composite.baseComponent;

//base component
public interface Shape {
	public void draw(String color);
}
