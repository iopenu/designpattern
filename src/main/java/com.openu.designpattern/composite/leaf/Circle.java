package com.openu.designpattern.composite.leaf;

import com.openu.designpattern.composite.baseComponent.Shape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//leaf
public class Circle implements Shape {
	private final static Logger logger = LoggerFactory.getLogger(Circle.class);

	@Override
	public void draw(String color) {
		logger.info("circle color : {}", color);
	}
}
