package com.openu.designpattern.composite.leaf;

import com.openu.designpattern.composite.baseComponent.Shape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Line implements Shape {
	private final static Logger logger = LoggerFactory.getLogger(Line.class);
	@Override
	public void draw(String color) {
		logger.info("line color : {}", color);
	}
}
