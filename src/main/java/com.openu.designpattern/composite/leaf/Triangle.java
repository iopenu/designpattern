package com.openu.designpattern.composite.leaf;

import com.openu.designpattern.composite.baseComponent.Shape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//leaf
public class Triangle implements Shape {
	private final static Logger logger = LoggerFactory.getLogger(Triangle.class);

	@Override
	public void draw(String color) {
		logger.info("triangle color : {}", color);
	}
}
