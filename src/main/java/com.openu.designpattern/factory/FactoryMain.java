package com.openu.designpattern.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FactoryMain {
	private final static Logger logger = LoggerFactory.getLogger(FactoryMain.class);

	public static void main(String[] args) {

		Product t1 = ProductFactory.getProduct("ticket","한국여행",30000);
		Product t2 = ProductFactory.getProduct("computer","pc",100000);

		logger.debug("t1 | {}", t1.toString());
		logger.debug("t2 | {}", t2.toString());

	}
}
