package com.openu.designpattern.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Product {
	private final static Logger logger = LoggerFactory.getLogger(Product.class);

	public abstract String getName();
	public abstract int getPrice();

	@Override
	public String toString(){
		return "product name : "+getName() + ", price : "+getPrice();
	}
}
