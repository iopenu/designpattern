package com.openu.designpattern.factory;

public class ProductFactory {
	public static Product getProduct(String type,String name,int price){
		if(null==type){
			return null;
		}else if("ticket".equalsIgnoreCase(type)){
			return new Ticket(name,price);
		}else if("computer".equalsIgnoreCase(type)){
			return new Computer(name,price);
		}else{
			return null;
		}
	}
}
