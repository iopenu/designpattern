package com.openu.designpattern.singleton.EnumInitialization;

public enum EnumInitialization {
	INSTANCE;
	static String test = "";
	public static EnumInitialization getInstance(){
		test = "test";
		return INSTANCE;
	}
}
