package com.openu.designpattern.singleton.EnumInitialization;

public class EnumInitializationMain {
	public static void main(String[] args) {
		EnumInitialization instance = EnumInitialization.getInstance();
		System.out.println(instance.hashCode());
	}
}
