package com.openu.designpattern.singleton.InitializationOnDemandHolderIdiom;

	//jvm의 class loader와 class의 load시점을 이용하여 내부 class를 생성시킴`
public class InitializationOnDemandHolderIdiom {
	private InitializationOnDemandHolderIdiom(){
		System.out.println("constructor init initializationOnDemandHolder");
	}
	private static class Singleton {
		private static final InitializationOnDemandHolderIdiom instance = new InitializationOnDemandHolderIdiom();
	}
	public static InitializationOnDemandHolderIdiom getInstance(){
		System.out.println("call getInstance");
		return Singleton.instance;
	}
}
