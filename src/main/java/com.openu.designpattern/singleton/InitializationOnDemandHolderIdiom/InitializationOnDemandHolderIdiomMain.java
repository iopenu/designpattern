package com.openu.designpattern.singleton.InitializationOnDemandHolderIdiom;

public class InitializationOnDemandHolderIdiomMain {
	public static void main(String[] args) {
		InitializationOnDemandHolderIdiom instance1 = InitializationOnDemandHolderIdiom.getInstance();
		InitializationOnDemandHolderIdiom instance2 = InitializationOnDemandHolderIdiom.getInstance();

		System.out.println(instance1);
		System.out.println(instance2);
	}


}
