package com.openu.designpattern.singleton.normal;

public class NormalSingleton {
	// private static 선언
	private static NormalSingleton instance;
	//생성자
	private NormalSingleton(){
		System.out.println("call normal Singleton!");
	}

	//하지만 multy thread 이면 ㅜㅜ
	//public static synchronized NormalSingleton getInstance(){
	//synchronized 를 하지만 많은 thread들이 getInstance를 호출하면 성능저하
	public static NormalSingleton getInstance(){
		if(instance == null){
			instance = new NormalSingleton();
		}
		return instance;
	}

}
