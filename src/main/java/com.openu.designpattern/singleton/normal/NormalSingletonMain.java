package com.openu.designpattern.singleton.normal;

public class NormalSingletonMain {
	public static void main(String[] args) {
		NormalSingleton instance1 = NormalSingleton.getInstance();
		NormalSingleton instance2 = NormalSingleton.getInstance();

		System.out.println(instance1);
		System.out.println(instance2);
	}

}
