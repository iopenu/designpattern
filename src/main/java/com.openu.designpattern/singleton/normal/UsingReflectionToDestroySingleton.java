package com.openu.designpattern.singleton.normal;

import java.lang.reflect.Constructor;

public class UsingReflectionToDestroySingleton {
	public static void main(String[] args) {
		NormalSingleton instance = NormalSingleton.getInstance();
		NormalSingleton instance2 = null;

		try {
			Constructor[] constructors = NormalSingleton.class.getDeclaredConstructors();
			for(Constructor constructor : constructors ) {
				constructor.setAccessible(true);
				instance2 = (NormalSingleton)constructor.newInstance();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		System.out.println(instance.hashCode());
		System.out.println(instance2.hashCode());

	}
}
