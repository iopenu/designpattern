package com.openu.designpattern.singleton.staticblock;

public class StaticBlockInitialization {
	private static StaticBlockInitialization instance;
	private StaticBlockInitialization(){}

	static {
		try {
			System.out.println("Crate Static Block initialization");
			if(null==instance) {
				instance = new StaticBlockInitialization();
			}
		}catch (Exception e) {
			throw new RuntimeException("Exception crating StaticBlockInitialzation");
		}
	}

	public static StaticBlockInitialization getInstance(){
		return instance;
	}
}
