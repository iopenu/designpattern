package com.openu.designpattern.singleton.staticblock;

public class StaticBlockInitializationMain {
	public static void main(String[] args) {
		StaticBlockInitialization instance1 = StaticBlockInitialization.getInstance();
		StaticBlockInitialization instance2 = StaticBlockInitialization.getInstance();

		System.out.println(instance1);
		System.out.println(instance2);
	}


}
