package com.openu.designpattern.sort;

public class BubbleSorter {
	static int operations = 0;
	
	public static int sort(int[] array){
		operations = 0;
		
		if(array.length <= 1)
			return operations;
		System.out.println("array.length = "+ array.length);
		//for(4;4>=0 ;4-1)
		for(int nextToLast = array.length - 2; nextToLast >=0 ; nextToLast--){
			System.out.println("nextToLast = "+ nextToLast);
			for(int index = 0; index <= nextToLast ;index++){
				compareAndSwap(array,index);
			}
		}
		
		return operations;
	}

	private static void swap(int[] array, int index) {
		int temp = array[index];
		array[index] = array[index +1];
		array[index +1 ] = temp;
	}
	
	private static void compareAndSwap(int[] array, int index) {
		if(array[index]> array[index +1])
			swap(array,index);
		operations++;
	}
	
	public static void main(String[] args) {
		int[] array ={1,3,2,5,8,4};
		sort(array);
		System.out.println(operations);
		for(int temp : array){
			System.out.println(temp);
		}
		
	}
	
	

}
