package com.openu.designpattern.strategy.animal;


import com.openu.designpattern.strategy.animal.animal.Animal;
import com.openu.designpattern.strategy.animal.animal.Eagle;
import com.openu.designpattern.strategy.animal.animal.Tiger;
import com.openu.designpattern.strategy.animal.animal.Turtle;

public class AnimalMain {
	public static void main(String[] args) {
		Animal tiger = new Tiger();
		tiger.move();
		tiger.performCry();
		tiger.display();

		System.out.println("=============================");

		Animal eagle = new Eagle();
		eagle.move();
		eagle.performCry();
		eagle.display();
		System.out.println("=============================");

		Animal turtle = new Turtle();
		turtle.move();
		turtle.performCry();
		turtle.performFly();
		turtle.display();

	}
}
