package com.openu.designpattern.strategy.animal.animal;

import com.openu.designpattern.strategy.animal.cry.Cry;
import com.openu.designpattern.strategy.animal.fly.Fly;

public abstract class Animal {

	protected Fly fly;

	protected Cry cry;

	public Animal(){

	}

	public void performFly(){
		fly.fly();;
	}

	public void performCry(){
		cry.cry();
	}

//	public void cry(){
//		System.out.println("우네");
//	}

	public void move(){
		System.out.println("움직이네");
	}

	public abstract void display();
//	{
	//	System.out.println("모습 display");
//	}
}
