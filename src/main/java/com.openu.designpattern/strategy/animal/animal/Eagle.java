package com.openu.designpattern.strategy.animal.animal;

import com.openu.designpattern.strategy.animal.cry.BirdCry;
import com.openu.designpattern.strategy.animal.fly.FlyWithWing;

public class Eagle extends Animal {
	public Eagle(){
		cry = new BirdCry();
		fly = new FlyWithWing();
	}

//	public void cry(){
//		System.out.println("까악");
//	}

	public void display(){
		System.out.println("독수리");
	}
}
