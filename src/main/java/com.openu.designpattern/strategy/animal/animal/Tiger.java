package com.openu.designpattern.strategy.animal.animal;

import com.openu.designpattern.strategy.animal.cry.TigerCry;
import com.openu.designpattern.strategy.animal.fly.FlyNoway;

public class Tiger extends Animal {
	public Tiger(){
		cry = new TigerCry();
		fly = new FlyNoway();

	}

//	public void cry(){
//		System.out.println("어흥");
//	}

	public void display(){
		System.out.println("호랑이");
	}
}
