package com.openu.designpattern.strategy.animal.animal;

import com.openu.designpattern.strategy.animal.cry.CryNoWay;
import com.openu.designpattern.strategy.animal.fly.FlyNoway;

public class Turtle extends Animal {

	public Turtle() {
		cry = new CryNoWay();
		fly = new FlyNoway();
	}
	@Override
	public void display() {
		System.out.println("거북이입니다.");
	}


}
