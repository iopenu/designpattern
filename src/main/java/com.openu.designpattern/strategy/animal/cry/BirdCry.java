package com.openu.designpattern.strategy.animal.cry;

public class BirdCry implements Cry{
	@Override
	public void cry() {
		System.out.println("짹짹");
	}
}
