package com.openu.designpattern.strategy.animal.cry;

public interface Cry {
	public void cry();
}
