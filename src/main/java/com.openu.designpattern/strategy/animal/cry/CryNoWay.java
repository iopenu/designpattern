package com.openu.designpattern.strategy.animal.cry;

public class CryNoWay implements Cry {

	@Override
	public void cry() {
		System.out.println("안올어");
	}
}
