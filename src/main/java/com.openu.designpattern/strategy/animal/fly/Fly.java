package com.openu.designpattern.strategy.animal.fly;

public interface Fly {
	public void fly();
}
