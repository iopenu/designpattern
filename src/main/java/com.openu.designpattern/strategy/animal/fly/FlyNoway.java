package com.openu.designpattern.strategy.animal.fly;

public class FlyNoway implements Fly  {
	@Override
	public void fly() {
		System.out.println("날지못한다");
	}
}
