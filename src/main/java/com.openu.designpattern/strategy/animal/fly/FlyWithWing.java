package com.openu.designpattern.strategy.animal.fly;

public class FlyWithWing implements Fly {

	@Override
	public void fly() {
		System.out.println("날개로 날다");
	}
}
