package com.openu.designpattern.strategy.normal;

public class DelegateMain {

	public static void main(String[] args) {
		DelegateObjA delegateObjA = new DelegateObjA();
		delegateObjA.funcAA();
	}

}
