package com.openu.designpattern.strategy.normal;

public class DelegateObjA {
	
	Ainterface ainterface;
	
	public DelegateObjA(){
		ainterface = new AinterfaceImpl();
	}
	
	//기능이 필요해요  
	public void funcAA(){
		//~~ 기능이 필요합니다. 개발해주세요.
		ainterface.funcA();
		ainterface.funcA();
	}
}
