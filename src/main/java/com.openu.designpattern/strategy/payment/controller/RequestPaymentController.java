package com.openu.designpattern.strategy.payment.controller;

import com.openu.designpattern.strategy.payment.model.RequestPaymentVO;
import com.openu.designpattern.strategy.payment.model.ResultVO;
import com.openu.designpattern.strategy.payment.service.RequestPayment;

public class RequestPaymentController {
	RequestPayment requestPayment;
	
	public void setRequestPayment(RequestPayment requestPayment){
		this.requestPayment = requestPayment;
	}
	
	public ResultVO requestValidate(RequestPaymentVO requestPaymentVO){
		ResultVO result = new ResultVO();
		//delegate
		if(null==requestPayment){
			result.setCode("XXX1");
			result.setMsg("payment의 종류를 set해주세요.");
		}else{
			result = requestPayment.requestValidate(requestPaymentVO);
		}
		return result;
		
	}
	

}
