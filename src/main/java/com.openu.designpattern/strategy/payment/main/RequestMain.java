package com.openu.designpattern.strategy.payment.main;

import com.openu.designpattern.common.CommonUtil;
import com.openu.designpattern.strategy.payment.service.impl.RequestPaymentCard;
import com.openu.designpattern.strategy.payment.controller.RequestPaymentController;
import com.openu.designpattern.strategy.payment.service.impl.RequestPaymentPhone;
import com.openu.designpattern.strategy.payment.model.RequestPaymentVO;
import com.openu.designpattern.strategy.payment.model.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RequestMain {
	private static final Logger logger = LoggerFactory.getLogger(RequestMain.class);
	public static void main(String[] args){

		//요청 컨트롤러
		RequestPaymentController requestPaymentController = new RequestPaymentController();
		
		//요청케이스1; 신용카드 승인.
		RequestPaymentVO requestVO = new RequestPaymentVO();
		
		//요청값 set 
		requestVO.setRequestID(CommonUtil.makeUUID());
		requestVO.setUserID("jane");
		requestVO.setPaymentmethod("phone");
		requestVO.setPaymentmethod("card");
		requestVO.setCurrency("jpy");
		requestVO.setAmount("1234");
		requestVO.setProductname("가전제품.");
		requestVO.setCardtype("master");
		requestVO.setCardno1("1111");
		requestVO.setCardno2("2222");
		requestVO.setCardno3("3333");
		requestVO.setCardno4("4444");
		requestVO.setExpiredYY("17");
		requestVO.setExpiredMM("02");
		requestVO.setCvv("1233");
		
		//start
		logger.debug("================================");
		logger.debug("REQUEST START DT : "+CommonUtil.nowDTFull());
		logger.debug("요청이 시작되었다.");
		
		//결제수단을 체크한다.
		if(requestVO.getPaymentmethod().equals("phone")){
			requestPaymentController.setRequestPayment(new RequestPaymentPhone());
		}else{
			requestPaymentController.setRequestPayment(new RequestPaymentCard());
		}
		
		//validate를 한다.
		ResultVO validateResult = requestPaymentController.requestValidate(requestVO);
		if(validateResult.getCode().equals("0000")){
			logger.debug("** validate Success");
		}else{
			logger.debug("** validate Fail : "+validateResult.getCode());
			logger.debug("고객에게 요청실패를 알린다.");
		}
		logger.debug("REQUEST END DT : "+CommonUtil.nowDTFull());
		logger.debug("----------- END   -----------------");

		logger.debug("================================");
		
	}

}
