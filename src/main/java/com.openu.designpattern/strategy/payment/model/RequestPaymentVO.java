package  com.openu.designpattern.strategy.payment.model;

import org.apache.commons.lang3.StringUtils;

//신용카드의 승인요청 vo

public class RequestPaymentVO {
	private String requestID ;			//요청 id					(필수)
	private String userID;				//userId				(필수)
	private String paymentmethod;		//결제방법 card, phone		(필수)
	private String currency;				//승인통화 krw,usd....		(필수)
	private String amount;				//승인요청금액				(필수)
	private String productname;			//제품명.					(선택)
	private String orderID;				//쇼핑몰의 주문번호     		(선택)
    
	private String cardtype;				//카드종류.
	private String cardno1;				//카드번호1 
	private String cardno2;				//카드번호2
	private String cardno3;				//카드번호3
	private String cardno4;				//카드번호4
	private String expiredYY;			//카드유효기간 yy(연도)
	private String expiredMM;			//카드유효기간 mm(month)
	private String cvv;					//cvv
    
	private String phonenoMiddle;		//전화번호 국번
	private String phonenoLast;			//전화번호 마지막자리
	private String phoneAuthNO;			//휴대폰결제 인증번호
    
	private String requestDT;			//요청시간
	private String responseDT;			//응답시간
	private String responseID;			//응답id
	private String status;				//상태  (1.요청정보저장, 2.validate, 3.requestAuth 4.response)
    
	
	
	public String toStrinCard() {
		return "RequestPaymentVO CARD [requestID=" + requestID + ", userID=" + userID + ", paymentmethod=" + paymentmethod
				+ ", currency=" + currency + ", amount=" + amount + ", productname=" + productname + ", cardtype="
				+ cardtype + ", cardno1=" + cardno1 + ", cardno2=" + cardno2 + ", cardno3=" + cardno3 + ", cardno4="
				+ cardno4 + ", expiredYY=" + expiredYY + ", expiredMM=" + expiredMM + ", cvv=" + cvv + ", responseID="
				+ responseID + "]";
	}
	
	public String toStringPhone() {
		return "RequestPaymentVO PHONE [requestID=" + requestID + ", userID=" + userID + ", paymentmethod=" + paymentmethod
				+ ", currency=" + currency + ", amount=" + amount + ", productname=" + productname + ", phonenoMiddle="
				+ phonenoMiddle + ", phonenoLast=" + phonenoLast + ", phoneAuthNO=" + phoneAuthNO + ", responseID="
				+ responseID + "]";
	}

	//getter setter
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		amount = amount.trim();
		//if(NumberUtil.isDouble(amount)){
		//	amount = amount.trim();
		//}else{
		//	amount = "0";
		//}
		this.amount = amount;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getCardtype() {
		return cardtype;
	}
	public void setCardtype(String cardtype) {
		cardtype = cardtype.trim().toUpperCase();
		this.cardtype = cardtype;
	}
	public String getExpiredYY() {
		return expiredYY;
	}
	public void setExpiredYY(String expiredYY) {
		this.expiredYY = expiredYY;
	}
	public String getExpiredMM() {
		return expiredMM;
	}
	public void setExpiredMM(String expiredMM) {
		if(StringUtils.isEmpty(expiredMM)){
			expiredMM = "00";
		}
		expiredMM = String.format("%02d", Integer.valueOf(expiredMM));
		this.expiredMM = expiredMM;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getPhonenoMiddle() {
		return phonenoMiddle;
	}
	public void setPhonenoMiddle(String phonenoMiddle) {
		this.phonenoMiddle = phonenoMiddle;
	}
	public String getPhonenoLast() {
		return phonenoLast;
	}
	public void setPhonenoLast(String phonenoLast) {
		this.phonenoLast = phonenoLast;
	}
	public String getPhoneAuthNO() {
		return phoneAuthNO;
	}
	public void setPhoneAuthNO(String phoneAuthNO) {
		this.phoneAuthNO = phoneAuthNO;
	}
	public String getRequestDT() {
		return requestDT;
	}
	public void setRequestDT(String requestDT) {
		this.requestDT = requestDT;
	}
	public String getResponseDT() {
		return responseDT;
	}
	public void setResponseDT(String responseDT) {
		this.responseDT = responseDT;
	}
	public String getResponseID() {
		return responseID;
	}
	public void setResponseID(String responseID) {
		this.responseID = responseID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setCurrency(String currency) {
		if(currency.length()==3){
			currency = currency.toUpperCase();
		}else{
			currency = "KRW";
		}
		this.currency = currency;
	}
	public String getCurrency() {
		return currency;
	}
	public String getCardno1() {
		return cardno1;
	}
	public void setCardno1(String cardno1) {
		this.cardno1 = cardno1;
	}
	public String getCardno2() {
		return cardno2;
	}
	public void setCardno2(String cardno2) {
		this.cardno2 = cardno2;
	}
	public String getCardno3() {
		return cardno3;
	}
	public void setCardno3(String cardno3) {
		this.cardno3 = cardno3;
	}
	public String getCardno4() {
		return cardno4;
	}
	public void setCardno4(String cardno4) {
		this.cardno4 = cardno4;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	};
    
    
}
