package  com.openu.designpattern.strategy.payment.model;

public class ResultVO {
	private String code = "0000";
	private String msg ="success!";
	private String type = "1";
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
