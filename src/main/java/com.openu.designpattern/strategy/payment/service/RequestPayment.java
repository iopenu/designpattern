package com.openu.designpattern.strategy.payment.service;

import com.openu.designpattern.strategy.payment.model.RequestPaymentVO;
import com.openu.designpattern.strategy.payment.model.ResultVO;

public interface RequestPayment {
	//TODO 카드 승인요청을 db에 저장한다.
	public int requestAdd();
	//카드 승인요청의 파라미터 값을 체크한다.
	public ResultVO requestValidate(RequestPaymentVO requestPaymentVO);

}
