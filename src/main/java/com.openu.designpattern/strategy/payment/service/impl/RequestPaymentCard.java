package com.openu.designpattern.strategy.payment.service.impl;

import com.openu.designpattern.common.CommonException;
import com.openu.designpattern.common.CommonUtil;
import com.openu.designpattern.common.CommonValidation;
import com.openu.designpattern.common.NumberUtil;
import com.openu.designpattern.strategy.payment.model.RequestPaymentVO;
import com.openu.designpattern.strategy.payment.model.ResultVO;
import com.openu.designpattern.strategy.payment.service.RequestPayment;


import java.util.Arrays;
import java.util.List;

public class RequestPaymentCard implements RequestPayment {

	@Override
	public int requestAdd() {

		return 0;
	}

	@Override
	public ResultVO requestValidate(RequestPaymentVO requestPaymentVO) {
		ResultVO result = new ResultVO();
		
		//요청값을 log로 남긴다.
		System.out.println(requestPaymentVO.toStrinCard());
		
		try{
			//** 필수 parameter를 체크한다.
			//공통 validation
			//step1-1. requestID check
			CommonValidation.checkRequestId(requestPaymentVO.getRequestID());
			//step1-2. userid check
			CommonValidation.checkUserId(requestPaymentVO.getUserID());
			//step1-3. currency check
			CommonValidation.checkCurrency(requestPaymentVO.getCurrency());
			//step1-4. amount check
			CommonValidation.checkAmount(requestPaymentVO.getAmount(),requestPaymentVO.getCurrency());
			
			//card일경우 validation
			//step2-1. card check
			checkCardType(requestPaymentVO.getCardtype());
			//step2-2. cardno check
			checkCardNo(requestPaymentVO.getCardno1(),requestPaymentVO.getCardno2(),requestPaymentVO.getCardno3(),requestPaymentVO.getCardno4(),requestPaymentVO.getCardtype());
			//step2-3. expiredDT check
			checkExpiredDT(requestPaymentVO.getExpiredMM(),requestPaymentVO.getExpiredYY());
			//step2-4. CVV check
			checkCVV(requestPaymentVO.getCvv());
			
		}catch(CommonException e){
			System.out.println(CommonUtil.nowDTFull()+" "+e.getMessage()+" ["+e.getErrorCode()+"]");
			result.setMsg(e.getMessage());
			result.setCode(e.getErrorCode());
		}
		
		return result;
	}
	
	//카드 cvv 유효성을 체크한다. errorcode : XCC4
	private void checkCVV(String cvv) throws CommonException{
		if(cvv.length()!=3){
			throw new CommonException("ERROR - [CARD_VALIDATE] cvv Illegal Argument :"+cvv,"XCC4");
		}
	}
	
	//카드 유효기간의 유효성을 체크한다. errorcode : XCC3
	private void checkExpiredDT(String expiredMM, String expiredYY) throws CommonException{
		if(NumberUtil.compare("01",expiredMM) || NumberUtil.compare(expiredMM,"12")){
			throw new CommonException("ERROR - [CARD_VALIDATE] expired Month range 01-12 :"+expiredMM,"XCC3");
		}
		if(NumberUtil.compare("17",expiredYY) || NumberUtil.compare(expiredYY,"99")){
			throw new CommonException("ERROR - [CARD_VALIDATE] expired YEAR range 17-99 :"+expiredYY,"XCC3");
		}
	}
	
	//카드 번호의 유효성을 체크한다. errorcode : XCC2
	private void checkCardNo(String cardno1, String cardno2, String cardno3, String cardno4,String cardtype) throws CommonException{
		if(cardno1.length()!=4 &&cardno2.length()!=4&& cardno3.length()!=4&&cardno4.length()!=4){
			throw new CommonException("ERROR - [CARD_VALIDATE] CARDNO Illegal Argument :"+cardno1+"-****-****-"+cardno4,"XCC2");
		}
	}

	//cardtype validation errorcode : XCC1
	private void checkCardType(String cardtype) throws CommonException{
		//지원하는카드		//TODO db에서 가지올것 
		List<String> cardTypes = Arrays.asList("VISA", "MASTER", "JCB");
		if(!cardTypes.contains(cardtype)){
			throw new CommonException("ERROR - [CARD_VALIDATE] CARDTYPE Unsupported :"+cardtype,"XCC1");
		}
	}
	
	



}
