package com.openu.designpattern.strategy.payment.service.impl;

import com.openu.designpattern.common.CommonException;
import com.openu.designpattern.common.CommonUtil;
import com.openu.designpattern.common.CommonValidation;
import com.openu.designpattern.strategy.payment.model.RequestPaymentVO;
import com.openu.designpattern.strategy.payment.model.ResultVO;
import com.openu.designpattern.strategy.payment.service.RequestPayment;


public class RequestPaymentPhone implements RequestPayment {

	@Override
	public int requestAdd() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ResultVO requestValidate(RequestPaymentVO requestPaymentVO) {
		ResultVO result = new ResultVO();
		
		//요청값을 log로 남긴다.
		System.out.println(requestPaymentVO.toStringPhone());
		
		try{
			//** 필수 parameter를 체크한다.
			//공통 validation
			//step1-1. requestID check
			CommonValidation.checkRequestId(requestPaymentVO.getRequestID());
			//step1-2. userid check
			CommonValidation.checkUserId(requestPaymentVO.getUserID());
			//step1-3. currency check
			CommonValidation.checkCurrency(requestPaymentVO.getCurrency());
			//step1-4. amount check
			CommonValidation.checkAmount(requestPaymentVO.getAmount(),requestPaymentVO.getCurrency());
			
			//휴대폰결제일경우 validation
			//step2-1. telno check
			checkTelNo(requestPaymentVO.getPhonenoMiddle(),requestPaymentVO.getPhonenoLast());
			//step2-2. phoneAuthNO check
			checkPhoneAuthNO(requestPaymentVO.getPhoneAuthNO());
			
		}catch(CommonException e){
			System.out.println(CommonUtil.nowDTFull()+" "+e.getMessage()+" ["+e.getErrorCode()+"]");
			result.setMsg(e.getMessage());
			result.setCode(e.getErrorCode());
		}
		
		return result;
	}

	//전화번호 인증번호를 체크한다. errorcode : XPC2
	private void checkPhoneAuthNO(String phoneAuthNO) throws CommonException{
		if(phoneAuthNO.length()!=4){
			throw new CommonException("ERROR - [PHONE_VALIDATE] PHONE AUTH NO Illegal Argument  :"+phoneAuthNO,"XPC2");
		}		
	}
	//전화번호의 유효성을  체크한다. errorcode : XPC1
	private void checkTelNo(String phonenoMiddle, String phonenoLast) throws CommonException{
		if(!(phonenoMiddle.length()==3 || phonenoMiddle.length()==4)){
			throw new CommonException("ERROR - [PHONE_VALIDATE] PHONE MIDDLE NO   Illegal Argument  :"+phonenoMiddle,"XPC1");
		}
		if(phonenoLast.length()!=4){
			throw new CommonException("ERROR - [PHONE_VALIDATE] PHONE LAST NO   Illegal Argument  :"+phonenoLast,"XPC1");
		}
	}


}
