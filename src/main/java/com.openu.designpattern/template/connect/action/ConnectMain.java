package com.openu.designpattern.template.connect.action;


import com.openu.designpattern.template.connect.logic.AbstConnectHelper;
import com.openu.designpattern.template.connect.logic.DefaultConnectHelper;

public class ConnectMain {
	
	public static void main(String[] args) {
		AbstConnectHelper helper = new DefaultConnectHelper();
		helper.requestConnection("test", "222");
	}

}
