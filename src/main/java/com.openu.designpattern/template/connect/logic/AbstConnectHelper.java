package com.openu.designpattern.template.connect.logic;

public abstract class AbstConnectHelper {
	protected abstract String doSecurity(String password);
	protected abstract boolean authentication(String id,String password);
	protected abstract int authorization(String username);
	protected abstract String connection(String info);
	
	public String requestConnection(String id,String password){
		
		//보안 작업 - 입력된 password를 aes256를 이용 암호화한다.
		String encodePassword = doSecurity(password);
		//인증과정 
		if(!authentication(id,encodePassword)){
			throw new Error("인증이 실패하였습니다.");
		}
		//권한과정 
		String username = "userName";
		int level = authorization(username);
		
		//접속 
		switch (level) {
		case -1:
			throw new Error("보다 낳은 작업을 위해 서버를 점검하고 있습니다..");
		case 0:
			System.out.println("개발 관리자 ");
			break;
		case 1:
			System.out.println("운용 관리자 ");
			break;
		case 2:
			System.out.println("영업 관리자 ");
			break;
		case 3:
			System.out.println("협력업체 ");
			break;
		case 4:
			System.out.println("소비자  ");
			break;
		case 5:
			System.out.println("권한없음");
			break;
		default:
			System.out.println("예외상황");
			break;
		}
		
		return connection(username );
	}

}
 
