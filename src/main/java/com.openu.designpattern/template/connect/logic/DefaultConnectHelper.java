package com.openu.designpattern.template.connect.logic;

public class DefaultConnectHelper extends AbstConnectHelper {

	@Override
	protected String doSecurity(String password) {
		System.out.println("평문password를 암호화한다.");
		//aes256 TODO
		//String encodePassword = aes256(password);
		String encodePassword = "encodePassword";
		return encodePassword;
	}

	@Override
	protected boolean authentication(String id, String password) {
		System.out.println("디비에 접속하여 id와 암호화된 password를 체크한다.");
		return true;
	}

	@Override
	protected int authorization(String username) {
		System.out.println("권한을 가져온다.");
		
		//추가 요청사항 
		//서버작업에는 접속을 막는다. case -1 
		return 0;
	}

	@Override
	protected String connection(String info) {
		System.out.println("접속페이지로 전달을 한다..");
		return "hi";
	}

}
